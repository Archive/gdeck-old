#include "gdeck-slot.h"
#include "gdeck-card.h"

#define GDECK_GET_SLOT_CLASS(slot_item) (GDECK_SLOT_ITEM_CLASS(GTK_OBJECT(slot_item)->klass))

static void
gdeck_slot_relayout (GDeckSlot *slot)
{
	GList *list = NULL;
	GnomeCanvasItem *item;
	gint count = 0;
	gint i = 0, j = 0;
	gint offset = GDECK_GET_SLOT_CLASS (slot->item)->offset_val;

	if (slot->card_list == NULL) {
		gnome_canvas_item_show (GNOME_CANVAS_ITEM (slot->item));
		return;
	} else {
		gnome_canvas_item_hide (GNOME_CANVAS_ITEM (slot->item));
	}

	switch (slot->type) {
	case GDECK_SLOT_NORMAL:
		for (list = slot->card_list; list != slot->card_list_end; list = list->next)
			gdeck_card_destroy_item ((GDeckCard *) list->data);
		item = gdeck_card_ensure_item ((GDeckCard *) list->data, slot->group);
		gtk_object_set (GTK_OBJECT (item), "x", 0.0, "y", 0.0, NULL);
		return;
	case GDECK_SLOT_RIGHT:
		list = slot->card_list;
		i = 1.0;
		j = 0.0;
		break;
	case GDECK_SLOT_PARTIAL_RIGHT:
		list = slot->card_list;
		i = 1.0;
		j = 0.0;
		break;
	case GDECK_SLOT_DOWN:
		list = slot->card_list;
		i = 0.0;
		j = 1.0;
		break;
	case GDECK_SLOT_PARTIAL_DOWN:
		list = slot->card_list;
		i = 0.0;
		j = 1.0;
		break;
	case GDECK_SLOT_LEFT:
		list = slot->card_list;
		i = -1.0;
		j = 0.0;
		break;
	case GDECK_SLOT_PARTIAL_LEFT:
		list = slot->card_list;
		i = -1.0;
		j = 0.0;
		break;
	case GDECK_SLOT_UP:
		list = slot->card_list;
		i = 0.0;
		j = -1.0;
		break;
	case GDECK_SLOT_PARTIAL_UP:
		list = slot->card_list;
		i = 0.0;
		j = -1.0;
		break;
	};

	for (; list; list = list->next) {
		item = gdeck_card_ensure_item ((GDeckCard *)list->data, slot->group);
		gtk_object_set (GTK_OBJECT (item),
				"x", (gdouble) i * count * offset,
				"y", (gdouble) j * count * offset,
				NULL);
		count++;
	}
}

GDeckSlot *
gdeck_slot_new (GnomeCanvas   *table,
		GDeckSlotType  type,
		gboolean       visible)
{
	GDeckSlot *retval;
	retval = g_new0 (GDeckSlot, 1);
	retval->type = type;
	retval->visible = visible;

	retval->table = table;
	retval->group = (GnomeCanvasGroup *)
		gnome_canvas_item_new (gnome_canvas_root (table),
				       GNOME_TYPE_CANVAS_GROUP,
				       "x", -100.0 - gdeck_card_width (),
				       "y", -100.0 - gdeck_card_height (),
				       NULL);
	retval->item = (GDeckSlotItem *)
		gnome_canvas_item_new (retval->group,
				       GDECK_TYPE_SLOT_ITEM,
				       "x", 0.0,
				       "y", 0.0,
				       "visible", visible,
				       NULL);
	retval->card_list = NULL;
	retval->card_list_end = NULL;
	return retval;
}

void
gdeck_slot_free (GnomeCanvas *table)
{
	g_warning ("we need to free memory\n");
}

void
gdeck_slot_set_offset (GDeckSlot *slot,
		       guint16    offset)
{
	if (slot->offset == offset)
		return;

	slot->offset = offset;
}

void
gdeck_slot_add_card (GDeckSlot *slot,
		     GDeckCard *card)
{
	g_return_if_fail (slot != NULL);
	g_return_if_fail (card != NULL);

	if (slot->card_list == NULL)
		slot->card_list = slot->card_list_end = g_list_prepend (NULL, card);
	else {
		g_list_append (slot->card_list_end, card);
		slot->card_list_end = g_list_next (slot->card_list_end);
	}

	card->slot = slot;
	gdeck_slot_relayout (slot);
}

void
gdeck_slot_add_card_stack (GDeckSlot *slot,
			   GList     *cards)
{
	g_return_if_fail (slot != NULL);
	g_return_if_fail (cards != NULL);

	slot->card_list = g_list_concat (slot->card_list,
					 cards);
	slot->card_list_end = g_list_last (slot->card_list);
	while (cards) {
		((GDeckCard *) cards->data)->slot = slot;
		cards = cards->next;
	}
	gdeck_slot_relayout (slot);
}

void
gdeck_slot_remove_card (GDeckSlot *slot,
			GDeckCard *card)
{

}

GList *
gdeck_slot_remove_card_stack (GDeckSlot *slot,
			      GDeckCard *base_card)
{
	GList *list;

	g_return_val_if_fail (slot != NULL, NULL);
	g_return_val_if_fail (base_card != NULL, NULL);

	list = g_list_find (slot->card_list, base_card);
	if (list == NULL)
		return NULL;

	if (list == slot->card_list) {
		slot->card_list = NULL;
		slot->card_list_end =NULL;
		gdeck_slot_relayout (slot);

		return list;
	}
	list->prev->next = NULL;
	slot->card_list_end = list->prev;
	list->prev = NULL;

	gdeck_slot_relayout (slot);
	return list;
}

void
gdeck_slot_fill_offset (GDeckSlot *slot)
{

}

gboolean
gdeck_slot_contains_point (GDeckSlot *slot,
			   gdouble    x,
			   gdouble    y)
{
	g_return_val_if_fail (slot != NULL, FALSE);

	gnome_canvas_item_w2i (GNOME_CANVAS_ITEM (slot->item), &x, &y);
	switch (slot->type) {
	case GDECK_SLOT_NORMAL:
	default:
		return ((GDECK_CARD_BASE (slot->item)->x < x) &&
			(GDECK_CARD_BASE (slot->item)->y < y) &&
			(GDECK_CARD_BASE (slot->item)->x + GDECK_CARD_BASE (slot->item)->width > x) &&
			(GDECK_CARD_BASE (slot->item)->y + GDECK_CARD_BASE (slot->item)->height > y));
	}
}
