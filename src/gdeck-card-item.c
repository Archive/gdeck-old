#include "gdeck.h"
#include "gdeck-card-item.h"
#include "gdeck-image.h"
#include <gdk-pixbuf/gdk-pixbuf.h>
#include <gdk-pixbuf/gdk-pixbuf.h>
#include <libart_lgpl/art_rgb_rgba_affine.h>
#include <libgnomeui/gnome-canvas-util.h>
#include <math.h>

enum {
	ARG_0,
	ARG_CARD_TYPE
};

static void gdeck_card_item_init       (GDeckCardItem       *card);
static void gdeck_card_item_class_init (GDeckCardItemClass  *class);
static void gdeck_card_item_destroy    (GtkObject       *object);
static void gdeck_card_item_set_arg    (GtkObject       *object,
				   GtkArg          *arg,
				   guint            arg_id);
static void gdeck_card_item_get_arg    (GtkObject       *object,
				   GtkArg          *arg,
				   guint            arg_id);
static void gdeck_card_item_render     (GnomeCanvasItem *item,
				   GnomeCanvasBuf  *buf);

static GDeckCardBaseClass *parent_class;

GtkType
gdeck_card_item_get_type (void)
{
	static GtkType card_type = 0;

	if (!card_type) {
		GtkTypeInfo card_info = {
			"GDeckCardItem",
			sizeof (GDeckCardItem),
			sizeof (GDeckCardItemClass),
			(GtkClassInitFunc) gdeck_card_item_class_init,
			(GtkObjectInitFunc) gdeck_card_item_init,
			NULL, /* reserved_1 */
			NULL, /* reserved_2 */
			(GtkClassInitFunc) NULL
		};

		card_type = gtk_type_unique (GDECK_TYPE_CARD_BASE, &card_info);
	}

	return card_type;
}

static void
gdeck_card_item_init (GDeckCardItem *card_item)
{

}

static void
gdeck_card_item_class_init (GDeckCardItemClass *class)
{
	GtkObjectClass *object_class;
	GnomeCanvasItemClass *item_class;

	object_class = (GtkObjectClass *) class;
	item_class = (GnomeCanvasItemClass *) class;

	parent_class = gtk_type_class (GDECK_TYPE_CARD_BASE);

	gtk_object_add_arg_type ("GDeckCardItem::card_type", GTK_TYPE_ENUM, GTK_ARG_READWRITE, ARG_CARD_TYPE);

	object_class->destroy = gdeck_card_item_destroy;
	object_class->set_arg = gdeck_card_item_set_arg;
	object_class->get_arg = gdeck_card_item_get_arg;

	item_class->render = gdeck_card_item_render;
}

static void
gdeck_card_item_destroy (GtkObject *object)
{
	GDeckCardItem *card;

	card = GDECK_CARD_ITEM (object);

	if (GTK_OBJECT_CLASS (parent_class)->destroy)
		(* GTK_OBJECT_CLASS (parent_class)->destroy) (object);
}

static void
gdeck_card_item_set_arg (GtkObject *object,
		    GtkArg    *arg,
		    guint      arg_id)
{
	GnomeCanvasItem *item;
	GDeckCardItem *card;

	item = GNOME_CANVAS_ITEM (object);
	card = GDECK_CARD_ITEM (object);

	switch (arg_id) {
	case ARG_CARD_TYPE:
/*		card->type = GTK_VALUE_ENUM (*arg);*/
		/* FIXME */
		break;
	default:
		break;
	}
}

static void
gdeck_card_item_get_arg    (GtkObject *object,
		       GtkArg    *arg,
		       guint      arg_id)
{
	GDeckCardItem *card;

	card = GDECK_CARD_ITEM (object);

	switch (arg_id) {
	case ARG_CARD_TYPE:
/*		GTK_VALUE_ENUM (*arg) = card->type;*/
		break;
	default:
		arg->type = GTK_TYPE_INVALID;
		break;
	}
}

static void
gdeck_card_item_draw_image (GnomeCanvasItem *item,
			    GnomeCanvasBuf  *buf,
			    GdkPixbuf       *pixbuf,
			    gint             x_offset,
			    gint             y_offset)
{
	gdouble i2c[6], viewport_affine[6], render_affine[6];
	
	gnome_canvas_item_i2c_affine (item, i2c);
	art_affine_translate (viewport_affine,
			      GDECK_CARD_BASE (item)->x + x_offset,
			      GDECK_CARD_BASE (item)->y + y_offset);
	art_affine_multiply (render_affine, viewport_affine, i2c);

	art_rgb_rgba_affine (buf->buf,
			     buf->rect.x0, buf->rect.y0,
			     buf->rect.x1, buf->rect.y1,
			     buf->buf_rowstride,
			     gdk_pixbuf_get_pixels (pixbuf),
			     gdk_pixbuf_get_width (pixbuf),
			     gdk_pixbuf_get_height (pixbuf),
			     gdk_pixbuf_get_rowstride (pixbuf),
			     render_affine,
			     ART_FILTER_NEAREST, NULL);
}

static void
gdeck_card_item_render (GnomeCanvasItem *item,
		   GnomeCanvasBuf  *buf)
{
	GDeckCardItem *card;
	GdkPixbuf *pixbuf;

	card = GDECK_CARD_ITEM (item);
	pixbuf = gdeck_image_get_back ();

	if (GNOME_CANVAS_ITEM_CLASS (parent_class)->render)
		(* GNOME_CANVAS_ITEM_CLASS (parent_class)->render) (item, buf);

        gnome_canvas_buf_ensure_buf (buf);
	if (!card->face_up)
		gdeck_card_item_draw_image (item, buf, pixbuf, 5, 5);
	buf->is_bg = 0;

}
