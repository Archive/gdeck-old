#include "gdeck.h"
#include "gdeck-image.h"


/* generic functions */
void
gdeck_init (void)
{
	gtk_widget_push_visual (gdk_rgb_get_visual ());
	gtk_widget_push_colormap (gdk_rgb_get_cmap ());

	gdeck_image_init ();
}

gdouble
gdeck_card_width (void)
{
	return 75.0;
}

gdouble
gdeck_card_height (void)
{
	return 100.0;
}

