/*
 * gdeck-image.h
 *
 * Copyright (C) 2000 Jonathan Blandford  <jrb@redhat.com>
 * All rights reserved.
 *
 * This file is part of the gdeck Library.
 *
 * The Gnome Library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * The Gnome Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with the Gnome Library; see the file COPYING.LIB.  If not,
 * write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef __GDECK_IMAGE_H__
#define __GDECK_IMAGE_H__

void       gdeck_image_init      (void);
GdkPixbuf *gdeck_image_get_suit  (GDeckSuit     suit,
				  GDeckSizes    size);
GdkPixbuf *gdeck_image_get_back  (void);
GdkPixbuf *gdeck_image_get_value (GDeckValue    value);
GdkPixbuf *gdeck_image_get_slot  (GDeckSlotType type);



#endif
