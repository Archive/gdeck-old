/*
 * Copyright (C) 2000 Jonathan Blandford  <jrb@redhat.com>
 * All rights reserved.
 *
 * This file is part of the gdeck Library.
 *
 * The Gnome Library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * The Gnome Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with the Gnome Library; see the file COPYING.LIB.  If not,
 * write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef __GDECK_SLOT_H__
#define __GDECK_SLOT_H__

#include "gdeck.h"
#include "gdeck-slot-item.h"
#include "gdeck-card.h"

struct _GDeckSlot {
	GDeckSlotType type;
	gboolean visible;
	guint16 offset;
	guint16 real_offset;

	GnomeCanvas *table;
	GnomeCanvasGroup *group;
	GDeckSlotItem *item;
	
	GList *card_list;
	GList *card_list_end;
};


GDeckSlot *gdeck_slot_new               (GnomeCanvas   *table,
					 GDeckSlotType  type,
					 gboolean       visible);
void       gdeck_slot_free              (GnomeCanvas   *table);
void       gdeck_slot_set_offset        (GDeckSlot     *slot,
					 guint16        offset);
void       gdeck_slot_add_card          (GDeckSlot     *slot,
					 GDeckCard     *card);
void       gdeck_slot_add_card_stack    (GDeckSlot     *slot,
					 GList         *cards);
void       gdeck_slot_remove_card       (GDeckSlot     *slot,
					 GDeckCard     *card);
GList     *gdeck_slot_remove_card_stack (GDeckSlot     *slot,
					 GDeckCard     *base_card);
void       gdeck_slot_fill_offset       (GDeckSlot     *slot);
gboolean   gdeck_slot_contains_point    (GDeckSlot     *slot,
					 gdouble        x,
					 gdouble        y);



#endif
