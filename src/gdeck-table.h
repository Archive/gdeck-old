/*
 * gdeck-table.h
 *
 * Copyright (C) 2000 Jonathan Blandford  <jrb@redhat.com>
 * All rights reserved.
 *
 * This file is part of the gdeck Library.
 *
 * The Gnome Library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * The Gnome Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with the Gnome Library; see the file COPYING.LIB.  If not,
 * write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef __GDECK_TABLE_H__
#define __GDECK_TABLE_H__

#include "gdeck-slot.h"

#define GDECK_TYPE_TABLE            (gdeck_table_get_type ())
#define GDECK_TABLE(obj)            (GTK_CHECK_CAST ((obj), GDECK_TYPE_TABLE, GDeckTable))
#define GDECK_TABLE_CLASS(klass)    (GTK_CHECK_CLASS_CAST ((klass), GDECK_TYPE_TABLE, GDeckTableClass))
#define GDECK_IS_TABLE(obj)         (GTK_CHECK_TYPE ((obj), GDECK_TYPE_TABLE))
#define GDECK_IS_TABLE_CLASS(klass) (GTK_CHECK_CLASS_TYPE ((klass), GDECK_TYPE_TABLE))


typedef struct _GDeckTable GDeckTable;
typedef struct _GDeckTableClass GDeckTableClass;

typedef enum {
	GDECK_EVENT_NONE,
	GDECK_EVENT_BUTTON_PRESS,
	GDECK_EVENT_MOVING
} GDeckEvents;

struct _GDeckTable {
	GnomeCanvas parent;

	GSList *slots;

	/* Card motion events */
	gdouble x_offset;
	gdouble y_offset;
	GDeckEvents event;
};

struct _GDeckTableClass {
	GnomeCanvasClass parent_class;
};

GtkType    gdeck_table_get_type       (void);
GtkWidget *gdeck_table_new            (void);
void       gdeck_table_set_background (GDeckTable *table,
				       GdkPixbuf  *bg);
void       gdeck_table_add_slot       (GDeckTable *table,
				       GDeckSlot  *slot,
				       gdouble     x,
				       gdouble     y);
GDeckSlot *gdeck_table_find_slot      (GDeckTable *table,
				       gdouble     x,
				       gdouble     y);


#endif
