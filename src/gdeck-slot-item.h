/*
 * Copyright (C) 2000 Jonathan Blandford  <jrb@redhat.com>
 * All rights reserved.
 *
 * This file is part of the gdeck Library.
 *
 * The Gnome Library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * The Gnome Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with the Gnome Library; see the file COPYING.LIB.  If not,
 * write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef __GDECK_SLOT_ITEM_H__
#define __GDECK_SLOT_ITEM_H__

#include "gdeck.h"
#include "gdeck-card-base.h"

#define GDECK_TYPE_SLOT_ITEM    	(gdeck_slot_item_get_type ())
#define GDECK_SLOT_ITEM(obj)            (GTK_CHECK_CAST ((obj), GDECK_TYPE_SLOT_ITEM, GDeckSlotItem))
#define GDECK_SLOT_ITEM_CLASS(klass)    (GTK_CHECK_CLASS_CAST ((klass), GDECK_TYPE_SLOT_ITEM, GDeckSlotItemClass))
#define GDECK_IS_SLOT_ITEM(obj)         (GTK_CHECK_TYPE ((obj), GDECK_TYPE_SLOT_ITEM))
#define GDECK_IS_SLOT_ITEM_CLASS(klass) (GTK_CHECK_CLASS_TYPE ((klass), GDECK_TYPE_SLOT_ITEM))


typedef struct _GDeckSlotItem GDeckSlotItem;
typedef struct _GDeckSlotItemClass GDeckSlotItemClass;

struct _GDeckSlotItem {
	GDeckCardBase parent;

	gboolean visible;
};

struct _GDeckSlotItemClass {
	GDeckCardBaseClass parent_class;

	gint offset_val;
};

GtkType        gdeck_slot_item_get_type       (void);


#endif
