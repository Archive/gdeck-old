/*
 * Copyright (C) 2000 Jonathan Blandford  <jrb@redhat.com>
 * All rights reserved.
 *
 * This file is part of the gdeck Library.
 *
 * The Gnome Library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * The Gnome Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with the Gnome Library; see the file COPYING.LIB.  If not,
 * write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef __GDECK_CARD_ITEM_H__
#define __GDECK_CARD_ITEM_H__


#include "gdeck.h"
#include "gdeck-card-base.h"

#define GDECK_TYPE_CARD_ITEM            (gdeck_card_item_get_type ())
#define GDECK_CARD_ITEM(obj)            (GTK_CHECK_CAST ((obj), GDECK_TYPE_CARD_ITEM, GDeckCardItem))
#define GDECK_CARD_ITEM_CLASS(klass)    (GTK_CHECK_CLASS_CAST ((klass), GDECK_TYPE_CARD_ITEM, GDeckCardItemClass))
#define GDECK_IS_CARD_ITEM(obj)         (GTK_CHECK_TYPE ((obj), GDECK_TYPE_CARD_ITEM))
#define GDECK_IS_CARD_ITEM_CLASS(klass) (GTK_CHECK_CLASS_TYPE ((klass), GDECK_TYPE_CARD_ITEM))



typedef struct _GDeckCardItem GDeckCardItem;
typedef struct _GDeckCardItemClass GDeckCardItemClass;

struct _GDeckCardItem {
	GDeckCardBase parent;

	gboolean face_up;
	GDeckCard *card;
};

struct _GDeckCardItemClass {
	GDeckCardBaseClass parent_class;
};


GtkType        gdeck_card_item_get_type (void);


#endif
