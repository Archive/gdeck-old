#include <gnome.h>
#include "gdeck.h"
#include "gdeck-slot-item.h"
#include "gdeck-card-item.h"
#include "gdeck-table.h"

gint
main (gint argc, gchar **argv)
{
	GtkWidget *window;
	GtkWidget *scroll;
	GtkWidget *table;

	GDeckSlot *slot;
	GList *deck;

	gnome_init ("AisleRiot", "2.0", argc, argv);
	gdeck_init ();

	window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
	table = gdeck_table_new ();
	scroll = gtk_scrolled_window_new (GTK_LAYOUT (table)->hadjustment,
					  GTK_LAYOUT (table)->vadjustment);
	gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(scroll),
				       GTK_POLICY_AUTOMATIC,
				       GTK_POLICY_AUTOMATIC);
	gtk_container_add (GTK_CONTAINER (scroll), table);
	gtk_container_add (GTK_CONTAINER (window), scroll);
	gtk_signal_connect (GTK_OBJECT (window), "destroy", gtk_main_quit, NULL);

	gnome_canvas_set_scroll_region (GNOME_CANVAS (table),
					0.0, 0.0, 500.0, 500.0);

	slot = gdeck_slot_new (GNOME_CANVAS (table), GDECK_SLOT_PARTIAL_UP, TRUE);
	gdeck_table_add_slot (GDECK_TABLE (table), slot, 60.0, 60.0);

	slot = gdeck_slot_new (GNOME_CANVAS (table), GDECK_SLOT_UP, TRUE);
	gdeck_table_add_slot (GDECK_TABLE (table), slot, 160.0, 60.0);

	slot = gdeck_slot_new (GNOME_CANVAS (table), GDECK_SLOT_PARTIAL_RIGHT, TRUE);
	gdeck_table_add_slot (GDECK_TABLE (table), slot, 260.0, 60.0);

	slot = gdeck_slot_new (GNOME_CANVAS (table), GDECK_SLOT_LEFT, TRUE);
	gdeck_table_add_slot (GDECK_TABLE (table), slot, 60.0, 210.0);

	deck = gdeck_deck_new (TRUE);
	slot = gdeck_slot_new (GNOME_CANVAS (table), GDECK_SLOT_NORMAL, TRUE);
	gdeck_table_add_slot (GDECK_TABLE (table), slot, 160.0, 210.0);
	gdeck_slot_add_card_stack (slot, deck);

	slot = gdeck_slot_new (GNOME_CANVAS (table), GDECK_SLOT_RIGHT, TRUE);
	gdeck_table_add_slot (GDECK_TABLE (table), slot, 260.0, 210.0);

	slot = gdeck_slot_new (GNOME_CANVAS (table), GDECK_SLOT_PARTIAL_LEFT, TRUE);
	gdeck_table_add_slot (GDECK_TABLE (table), slot, 60.0, 360.0);

	slot = gdeck_slot_new (GNOME_CANVAS (table), GDECK_SLOT_DOWN, TRUE);
	gdeck_table_add_slot (GDECK_TABLE (table), slot, 160.0, 360.0);

	slot = gdeck_slot_new (GNOME_CANVAS (table), GDECK_SLOT_PARTIAL_DOWN, TRUE);
	gdeck_table_add_slot (GDECK_TABLE (table), slot, 260.0, 360.0);

	gtk_window_set_default_size (GTK_WINDOW (window), 500, 500);
	gtk_widget_show_all (window);
	gtk_main ();
	return 0;
}


