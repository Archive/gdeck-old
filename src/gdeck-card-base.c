/*
 * Copyright (C) 2000 Jonathan Blandford  <jrb@redhat.com>
 * All rights reserved.
 *
 * This file is part of the gdeck Library.
 *
 * The Gnome Library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * The Gnome Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with the Gnome Library; see the file COPYING.LIB.  If not,
 * write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
/*
 * Playing Card back item for GDeck library.
 *
 * This item is solely used for the AA canvas.
 */

#include <math.h>
#include "gdeck.h"
#include "gdeck-card-base.h"

#include <libgnomeui/gnome-canvas-util.h>

#include "libart_lgpl/art_vpath.h"
#include "libart_lgpl/art_bpath.h"
#include "libart_lgpl/art_vpath_bpath.h"
#include "libart_lgpl/art_svp.h"
#include "libart_lgpl/art_svp_vpath.h"
#include "libart_lgpl/art_rgb_svp.h"

#define HALFWIDTH 0.35

enum {
	ARG_0,
	ARG_X,
	ARG_Y,
	ARG_WIDTH,
	ARG_HEIGHT,
	ARG_RADIUS,
	ARG_FILL_COLOR,
	ARG_FILL_COLOR_RGBA,
	ARG_OUTLINE_COLOR,
	ARG_OUTLINE_COLOR_RGBA
};

static void   gdeck_card_base_class_init  (GDeckCardBaseClass  *class);
static void   gdeck_card_base_init        (GDeckCardBase       *re);
static void   gdeck_card_base_destroy     (GtkObject           *object);
static void   gdeck_card_base_set_arg     (GtkObject           *object,
					   GtkArg              *arg,
					   guint                arg_id);
static void   gdeck_card_base_get_arg     (GtkObject           *object,
					   GtkArg              *arg,
					   guint                arg_id);

static void   gdeck_card_base_update      (GnomeCanvasItem     *item,
					   double               affine[6],
					   ArtSVP              *clip_path,
					   gint                 flags);
static double gdeck_card_base_point       (GnomeCanvasItem     *item,
					   double               x,
					   double               y,
					   int                  cx,
					   int                  cy,
					   GnomeCanvasItem    **actual_item);
static void   gdeck_card_base_translate   (GnomeCanvasItem     *item,
					   double               dx,
					   double               dy);
static void   gdeck_card_base_bounds      (GnomeCanvasItem     *item,
					   double              *x1,
					   double              *y1,
					   double              *x2,
					   double              *y2);
static void   gdeck_card_base_render      (GnomeCanvasItem     *item,
					   GnomeCanvasBuf      *buf);

static void   gdeck_card_base_set_fill    (GDeckCardBase       *card,
					   gboolean             fill_set);
static void   gdeck_card_base_set_outline (GDeckCardBase       *card,
					   gboolean             outline_set);


static GnomeCanvasItemClass *parent_class;

GtkType
gdeck_card_base_get_type (void)
{
	static GtkType card_type = 0;

	if (!card_type) {
		GtkTypeInfo card_info = {
			"GDeckCardBase",
			sizeof (GDeckCardBase),
			sizeof (GDeckCardBaseClass),
			(GtkClassInitFunc) gdeck_card_base_class_init,
			(GtkObjectInitFunc) gdeck_card_base_init,
			NULL, /* reserved_1 */
			NULL, /* reserved_2 */
			(GtkClassInitFunc) NULL
		};

		card_type = gtk_type_unique (gnome_canvas_item_get_type (), &card_info);
	}

	return card_type;
}

static void
gdeck_card_base_class_init (GDeckCardBaseClass *class)
{
	GtkObjectClass *object_class;
	GnomeCanvasItemClass *item_class;

	object_class = (GtkObjectClass *) class;
	item_class = (GnomeCanvasItemClass *) class;

	parent_class = gtk_type_class (gnome_canvas_item_get_type ());

	gtk_object_add_arg_type ("GDeckCardBase::x", GTK_TYPE_DOUBLE, GTK_ARG_READWRITE, ARG_X);
	gtk_object_add_arg_type ("GDeckCardBase::y", GTK_TYPE_DOUBLE, GTK_ARG_READWRITE, ARG_Y);
	gtk_object_add_arg_type ("GDeckCardBase::width", GTK_TYPE_DOUBLE, GTK_ARG_READWRITE, ARG_WIDTH);
	gtk_object_add_arg_type ("GDeckCardBase::height", GTK_TYPE_DOUBLE, GTK_ARG_READWRITE, ARG_HEIGHT);
	gtk_object_add_arg_type ("GDeckCardBase::radius", GTK_TYPE_DOUBLE, GTK_ARG_READWRITE, ARG_RADIUS);
	gtk_object_add_arg_type ("GDeckCardBase::fill_color", GTK_TYPE_STRING, GTK_ARG_WRITABLE, ARG_FILL_COLOR);
	gtk_object_add_arg_type ("GDeckCardBase::fill_color_rgba", GTK_TYPE_UINT, GTK_ARG_READWRITE, ARG_FILL_COLOR_RGBA);
	gtk_object_add_arg_type ("GDeckCardBase::outline_color", GTK_TYPE_STRING, GTK_ARG_WRITABLE, ARG_OUTLINE_COLOR);
	gtk_object_add_arg_type ("GDeckCardBase::outline_color_rgba", GTK_TYPE_UINT, GTK_ARG_READWRITE, ARG_OUTLINE_COLOR_RGBA);

	object_class->destroy = gdeck_card_base_destroy;
	object_class->get_arg = gdeck_card_base_get_arg;
	object_class->set_arg = gdeck_card_base_set_arg;

	item_class->point = gdeck_card_base_point;
	item_class->update = gdeck_card_base_update;
	item_class->translate = gdeck_card_base_translate;
	item_class->bounds = gdeck_card_base_bounds;
	item_class->render = gdeck_card_base_render;
}

static void
gdeck_card_base_init (GDeckCardBase *card)
{
	/* we put it off screen so it can't be seen */
	card->x = -100.0 - gdeck_card_width ();
	card->y = -100.0 - gdeck_card_height ();
	card->radius = 5.0;
	card->width = gdeck_card_width ();
	card->height = gdeck_card_height ();
	card->fill_color = 0xFFFFFFFF;
	card->outline_color = 0x000000FF;
	card->fill_svp = NULL;
	card->outline_svp = NULL;
	card->fill_set = TRUE;
	card->outline_set = TRUE;
}

static void
gdeck_card_base_destroy (GtkObject *object)
{
	GDeckCardBase *card;

	g_return_if_fail (object != NULL);
	g_return_if_fail (GDECK_IS_CARD_BASE (object));

	card = GDECK_CARD_BASE (object);

	if (card->fill_svp)
		art_svp_free (card->fill_svp);

	if (card->outline_svp)
		art_svp_free (card->outline_svp);

	if (GTK_OBJECT_CLASS (parent_class)->destroy)
		(* GTK_OBJECT_CLASS (parent_class)->destroy) (object);
}

static void
gdeck_card_base_get_arg (GtkObject *object,
			 GtkArg    *arg,
			 guint      arg_id)
{
	GDeckCardBase *card;

	card = GDECK_CARD_BASE (object);

	switch (arg_id) {
	case ARG_X:
		GTK_VALUE_DOUBLE (*arg) = card->x;
		break;

	case ARG_Y:
		GTK_VALUE_DOUBLE (*arg) = card->y;
		break;

	case ARG_WIDTH:
		GTK_VALUE_DOUBLE (*arg) = card->width;
		break;

	case ARG_HEIGHT:
		GTK_VALUE_DOUBLE (*arg) = card->height;
		break;

	case ARG_RADIUS:
		GTK_VALUE_DOUBLE (*arg) = card->radius;
		break;


	case ARG_FILL_COLOR_RGBA:
		GTK_VALUE_UINT (*arg) = card->fill_color;
		break;

	case ARG_OUTLINE_COLOR_RGBA:
		GTK_VALUE_UINT (*arg) = card->outline_color;
		break;

	default:
		arg->type = GTK_TYPE_INVALID;
		break;
	}
}

static void
gdeck_card_base_set_arg (GtkObject *object,
			 GtkArg    *arg,
			 guint      arg_id)
{
	GnomeCanvasItem *item;
	GDeckCardBase *card;
	GdkColor color = { 0, 0, 0, 0, };

	item = GNOME_CANVAS_ITEM (object);
	card = GDECK_CARD_BASE (object);

	switch (arg_id) {
	case ARG_X:
		card->x = GTK_VALUE_DOUBLE (*arg);

		gnome_canvas_item_request_update (item);
		break;

	case ARG_Y:
		card->y = GTK_VALUE_DOUBLE (*arg);

		gnome_canvas_item_request_update (item);
		break;

	case ARG_WIDTH:
		card->width = GTK_VALUE_DOUBLE (*arg);

		gnome_canvas_item_request_update (item);
		break;

	case ARG_HEIGHT:
		card->height = GTK_VALUE_DOUBLE (*arg);

		gnome_canvas_item_request_update (item);
		break;

	case ARG_FILL_COLOR:
		if (GTK_VALUE_STRING (*arg) &&
		    gdk_color_parse (GTK_VALUE_STRING (*arg), &color))
			gdeck_card_base_set_fill (card, TRUE);
		else
			gdeck_card_base_set_fill (card, FALSE);
		
		card->fill_color = ((color.red & 0xff00) << 16 |
				    (color.green & 0xff00) << 8 |
				    (color.blue & 0xff00) |
				    0xff);
		gnome_canvas_item_request_redraw_svp (item, card->fill_svp);
		break;

	case ARG_FILL_COLOR_RGBA:
		gdeck_card_base_set_fill (card, TRUE);
		card->fill_color = GTK_VALUE_UINT (*arg);
		gnome_canvas_item_request_redraw_svp (item, card->fill_svp);
		break;

	case ARG_OUTLINE_COLOR:
		if (GTK_VALUE_STRING (*arg) &&
		    gdk_color_parse (GTK_VALUE_STRING (*arg), &color))
			gdeck_card_base_set_outline (card, TRUE);
		else
			gdeck_card_base_set_outline (card, FALSE);

		card->outline_color = ((color.red & 0xff00) << 16 |
				       (color.green & 0xff00) << 8 |
				       (color.blue & 0xff00) |
				       0xff);
		gnome_canvas_item_request_redraw_svp (item, card->outline_svp);
		break;


	case ARG_OUTLINE_COLOR_RGBA:
		gdeck_card_base_set_outline (card, TRUE);
		card->outline_color = GTK_VALUE_UINT (*arg);
		gnome_canvas_item_request_redraw_svp (item, card->outline_svp);
		break;

	default:
		break;
	}
}

static void
gdeck_card_base_set_fill (GDeckCardBase *card,
			  gboolean       fill_set)
{
	if (card->fill_set != fill_set) {
		card->fill_set = fill_set;
		gnome_canvas_item_request_update (GNOME_CANVAS_ITEM (card));
	}
}

static void
gdeck_card_base_set_outline (GDeckCardBase *card,
			     gboolean       outline_set)
{
	if (card->outline_set != outline_set) {
		card->outline_set = outline_set;
		gnome_canvas_item_request_update (GNOME_CANVAS_ITEM (card));
	}
}

static double
gdeck_card_base_point (GnomeCanvasItem  *item,
		       double            x,
		       double            y,
		       int               cx,
		       int               cy,
		       GnomeCanvasItem **actual_item)
{
	GDeckCardBase *card;
	double x1, y1, x2, y2;
	double hwidth;
	double dx, dy;
	double tmp;

	card = GDECK_CARD_BASE (item);

	*actual_item = item;

	/* Find the bounds for the rectangle plus its outline width */

	x1 = card->x;
	y1 = card->y;
	x2 = card->x + card->width;
	y2 = card->y + card->height;

	if (card->outline_set) {
		hwidth = HALFWIDTH;

		x1 -= hwidth;
		y1 -= hwidth;
		x2 += hwidth;
		y2 += hwidth;
	} else
		hwidth = 0.0;

	/* Is point inside rectangle (which can be hollow if it has no fill set)? */

	if ((x >= x1) && (y >= y1) && (x <= x2) && (y <= y2)) {
		if (card->fill_set || !card->outline_set)
			return 0.0;

		dx = x - x1;
		tmp = x2 - x;
		if (tmp < dx)
			dx = tmp;

		dy = y - y1;
		tmp = y2 - y;
		if (tmp < dy)
			dy = tmp;

		if (dy < dx)
			dx = dy;

		dx -= 2.0 * hwidth;

		if (dx < 0.0)
			return 0.0;
		else
			return dx;
	}

	/* Point is outside rectangle */

	if (x < x1)
		dx = x1 - x;
	else if (x > x2)
		dx = x - x2;
	else
		dx = 0.0;

	if (y < y1)
		dy = y1 - y;
	else if (y > y2)
		dy = y - y2;
	else
		dy = 0.0;

	return sqrt (dx * dx + dy * dy);
}

static void
gdeck_card_base_update (GnomeCanvasItem *item,
			double           affine[6],
			ArtSVP          *clip_path,
			gint             flags)
{
	GDeckCardBase *card;
	ArtBpath bpath[19];
	ArtVpath *vpath_temp;
	ArtVpath *vpath;
	double x0, y0, x1, y1;
	double radius;

	card = GDECK_CARD_BASE (item);

	if (GNOME_CANVAS_ITEM_CLASS (parent_class)->update)
		(* GNOME_CANVAS_ITEM_CLASS (parent_class)->update) (item, affine, clip_path, flags);

	x0 = card->x;
	y0 = card->y;
	x1 = card->x + GDECK_CARD_BASE (item)->width;
	y1 = card->y + GDECK_CARD_BASE (item)->height;
	radius = GDECK_CARD_BASE (item)->radius;
	
	gnome_canvas_item_reset_bounds (item);

	if (card->fill_set) {
		bpath[0].code = ART_MOVETO;
		bpath[0].x1 = x0;
		bpath[0].y1 = y0 + radius;
		bpath[0].x2 = x0;
		bpath[0].y2 = y0 + radius;
		bpath[0].x3 = x0;
		bpath[0].y3 = y0 + radius;
		bpath[1].code = ART_LINETO;
		bpath[1].x1 = x0;
		bpath[1].y1 = y0 + radius;
		bpath[1].x2 = x0;
		bpath[1].y2 = y1 - radius;
		bpath[1].x3 = x0;
		bpath[1].y3 = y1 - radius;
		bpath[2].code = ART_CURVETO;
		bpath[2].x1 = x0;
		bpath[2].y1 = y1 - radius;
		bpath[2].x2 = x0;
		bpath[2].y2 = y1;
		bpath[2].x3 = x0 + radius;
		bpath[2].y3 = y1;
		bpath[3].code = ART_LINETO;
		bpath[3].x1 = x0 + radius;
		bpath[3].y1 = y1;
		bpath[3].x2 = x1 - radius;
		bpath[3].y2 = y1;
		bpath[3].x3 = x1 - radius;
		bpath[3].y3 = y1;
		bpath[4].code = ART_CURVETO;
		bpath[4].x1 = x1 - radius;
		bpath[4].y1 = y1;
		bpath[4].x2 = x1;
		bpath[4].y2 = y1;
		bpath[4].x3 = x1;
		bpath[4].y3 = y1 - radius;
		bpath[5].code = ART_LINETO;
		bpath[5].x1 = x1;
		bpath[5].y1 = y1 - radius;
		bpath[5].x2 = x1;
		bpath[5].y2 = y0 + radius;
		bpath[5].x3 = x1;
		bpath[5].y3 = y0 + radius;
		bpath[6].code = ART_CURVETO;
		bpath[6].x1 = x1;
		bpath[6].y1 = y0 + radius;
		bpath[6].x2 = x1;
		bpath[6].y2 = y0;
		bpath[6].x3 = x1 - radius;
		bpath[6].y3 = y0;
		bpath[7].code = ART_LINETO;
		bpath[7].x1 = x1 - radius;
		bpath[7].y1 = y0;
		bpath[7].x2 = x0 + radius;
		bpath[7].y2 = y0;
		bpath[7].x3 = x0 + radius;
		bpath[7].y3 = y0;
		bpath[8].code = ART_CURVETO;
		bpath[8].x1 = x0 + radius;
		bpath[8].y1 = y0;
		bpath[8].x2 = x0;
		bpath[8].y2 = y0;
		bpath[8].x3 = x0;
		bpath[8].y3 = y0 + radius;
		bpath[9].code = ART_END;
		bpath[9].x1 = 0;
		bpath[9].y1 = 0;
		bpath[9].x2 = 0;
		bpath[9].y2 = 0;
		bpath[9].x3 = 0;
		bpath[9].y3 = 0;

		vpath_temp = art_bez_path_to_vec (bpath, 0.25);
		vpath = art_vpath_affine_transform (vpath_temp, affine);

		gnome_canvas_item_update_svp_clip (item, &card->fill_svp, art_svp_from_vpath (vpath), clip_path);
		art_free (vpath_temp);
	} else
		gnome_canvas_item_update_svp (item, &card->fill_svp, NULL);

	if (card->outline_set) {
		gfloat halfwidth = HALFWIDTH;

		bpath[0].code = ART_MOVETO;
		bpath[0].x1 = x0 - halfwidth;
		bpath[0].y1 = y0 + radius;
		bpath[0].x2 = x0 - halfwidth;
		bpath[0].y2 = y0 + radius;
		bpath[0].x3 = x0 - halfwidth;
		bpath[0].y3 = y0 + radius;
		bpath[1].code = ART_LINETO;
		bpath[1].x1 = x0 - halfwidth;
		bpath[1].y1 = y0 + radius;
		bpath[1].x2 = x0 - halfwidth;
		bpath[1].y2 = y1 - radius;
		bpath[1].x3 = x0 - halfwidth;
		bpath[1].y3 = y1 - radius;
		bpath[2].code = ART_CURVETO;
		bpath[2].x1 = x0 - halfwidth;
		bpath[2].y1 = y1 - radius;
		bpath[2].x2 = x0 - halfwidth;
		bpath[2].y2 = y1 + halfwidth;
		bpath[2].x3 = x0 + halfwidth + radius;
		bpath[2].y3 = y1 + halfwidth;
		bpath[3].code = ART_LINETO;
		bpath[3].x1 = x0 + radius;
		bpath[3].y1 = y1 + halfwidth;
		bpath[3].x2 = x1 - radius;
		bpath[3].y2 = y1 + halfwidth;
		bpath[3].x3 = x1 - radius;
		bpath[3].y3 = y1 + halfwidth;
		bpath[4].code = ART_CURVETO;
		bpath[4].x1 = x1 - radius;
		bpath[4].y1 = y1 + halfwidth;
		bpath[4].x2 = x1 + halfwidth;
		bpath[4].y2 = y1 + halfwidth;
		bpath[4].x3 = x1 + halfwidth;
		bpath[4].y3 = y1 - radius;
		bpath[5].code = ART_LINETO;
		bpath[5].x1 = x1 + halfwidth;
		bpath[5].y1 = y1 - radius;
		bpath[5].x2 = x1 + halfwidth;
		bpath[5].y2 = y0 + radius;
		bpath[5].x3 = x1 + halfwidth;
		bpath[5].y3 = y0 + radius;
		bpath[6].code = ART_CURVETO;
		bpath[6].x1 = x1 + halfwidth;
		bpath[6].y1 = y0 + radius;
		bpath[6].x2 = x1 + halfwidth;
		bpath[6].y2 = y0 - halfwidth;
		bpath[6].x3 = x1 - radius;
		bpath[6].y3 = y0 - halfwidth;
		bpath[7].code = ART_LINETO;
		bpath[7].x1 = x1 - radius;
		bpath[7].y1 = y0 - halfwidth;
		bpath[7].x2 = x0 + radius;
		bpath[7].y2 = y0 - halfwidth;
		bpath[7].x3 = x0 + radius;
		bpath[7].y3 = y0 - halfwidth;
		bpath[8].code = ART_CURVETO;
		bpath[8].x1 = x0 + radius;
		bpath[8].y1 = y0 - halfwidth;
		bpath[8].x2 = x0 - halfwidth;
		bpath[8].y2 = y0 - halfwidth;
		bpath[8].x3 = x0 - halfwidth;
		bpath[8].y3 = y0 + radius;

		/* now we need to reverse the path on the inside */
		bpath[9].code = ART_MOVETO;
		bpath[9].x1 = x0 + halfwidth;
		bpath[9].y1 = y0 + radius;
		bpath[9].x2 = x0 + halfwidth;
		bpath[9].y2 = y0 + radius;
		bpath[9].x3 = x0 + halfwidth;
		bpath[9].y3 = y0 + radius;
		bpath[10].code = ART_CURVETO;
		bpath[10].x1 = x0 + halfwidth;
		bpath[10].y1 = y0 + radius;
		bpath[10].x2 = x0 + 2 * halfwidth;
		bpath[10].y2 = y0 + 2 * halfwidth;
		bpath[10].x3 = x0 + radius;
		bpath[10].y3 = y0 + halfwidth;
		bpath[11].code = ART_LINETO;
		bpath[11].x1 = x0 + radius;
		bpath[11].y1 = y0 + halfwidth;
		bpath[11].x2 = x1 - radius;
		bpath[11].y2 = y0 + halfwidth;
		bpath[11].x3 = x1 - radius;
		bpath[11].y3 = y0 + halfwidth;
		bpath[12].code = ART_CURVETO;
		bpath[12].x1 = x1 - radius;
		bpath[12].y1 = y0 + halfwidth;
		bpath[12].x2 = x1 - 2 * halfwidth;
		bpath[12].y2 = y0 + 2 * halfwidth;
		bpath[12].x3 = x1 - halfwidth;
		bpath[12].y3 = y0 + radius;
		bpath[13].code = ART_LINETO;
		bpath[13].x1 = x1 - halfwidth;
		bpath[13].y1 = y0 + radius;
		bpath[13].x2 = x1 - halfwidth;
		bpath[13].y2 = y1 - radius;
		bpath[13].x3 = x1 - halfwidth;
		bpath[13].y3 = y1 - radius;
		bpath[14].code = ART_CURVETO;
		bpath[14].x1 = x1 - halfwidth;
		bpath[14].y1 = y1 - radius;
		bpath[14].x2 = x1 - 2 * halfwidth;
		bpath[14].y2 = y1 - 2 * halfwidth;
		bpath[14].x3 = x1 - radius;
		bpath[14].y3 = y1 - halfwidth;
		bpath[15].code = ART_LINETO;
		bpath[15].x1 = x1 - radius;
		bpath[15].y1 = y1 - halfwidth;
		bpath[15].x2 = x0 + radius;
		bpath[15].y2 = y1 - halfwidth;
		bpath[15].x3 = x0 + radius;
		bpath[15].y3 = y1 - halfwidth;
		bpath[16].code = ART_CURVETO;
		bpath[16].x1 = x0 + radius;
		bpath[16].y1 = y1 - halfwidth;
		bpath[16].x2 = x0 + 2 * halfwidth;
		bpath[16].y2 = y1 - 2 * halfwidth;
		bpath[16].x3 = x0 + halfwidth;
		bpath[16].y3 = y1 - radius;
		bpath[17].code = ART_LINETO;
		bpath[17].x1 = x0 + halfwidth;
		bpath[17].y1 = y1 - radius;
		bpath[17].x2 = x0 + halfwidth;
		bpath[17].y2 = y0 + radius;
		bpath[17].x3 = x0 + halfwidth;
		bpath[17].y3 = y0 + radius;
		bpath[18].code = ART_END;
		bpath[18].x1 = 0;
		bpath[18].y1 = 0;
		bpath[18].x2 = 0;
		bpath[18].y2 = 0;
		bpath[18].x3 = 0;
		bpath[18].y3 = 0;

		vpath_temp = art_bez_path_to_vec (bpath, 0.25);
		vpath = art_vpath_affine_transform (vpath_temp, affine);

		gnome_canvas_item_update_svp_clip (item, &card->outline_svp, art_svp_from_vpath (vpath), clip_path);
		art_free (vpath_temp);
	} else
		gnome_canvas_item_update_svp (item, &card->outline_svp, NULL);
}

static void
gdeck_card_base_translate (GnomeCanvasItem *item,
			   double           dx,
			   double           dy)
{
	GDeckCardBase *card;
	
	card = GDECK_CARD_BASE (item);

	card->x += dx;
	card->y += dy;

	gnome_canvas_item_request_update (item);
	
}

static void
gdeck_card_base_bounds (GnomeCanvasItem *item,
			double          *x1,
			double          *y1,
			double          *x2,
			double          *y2)
{
	GDeckCardBase *card;
	double hwidth = HALFWIDTH;

	card = GDECK_CARD_BASE (item);

	*x1 = card->x - hwidth;
	*y1 = card->y - hwidth;
	*x2 = card->x + card->width + hwidth;
	*y2 = card->y + card->width + hwidth;
}


static void
gdeck_card_base_render (GnomeCanvasItem *item,
			GnomeCanvasBuf  *buf)
{
	GDeckCardBase *card;

	card = GDECK_CARD_BASE (item);

	if (card->fill_svp != NULL) {
		gnome_canvas_render_svp (buf, card->fill_svp, card->fill_color);
	}

	if (card->outline_svp != NULL) {
		gnome_canvas_render_svp (buf, card->outline_svp, card->outline_color);
	}

	if (GNOME_CANVAS_ITEM_CLASS (parent_class)->render)
		(* GNOME_CANVAS_ITEM_CLASS (parent_class)->render) (item, buf);

}
