/*
 * Copyright (C) 2000 Jonathan Blandford  <jrb@redhat.com>
 * All rights reserved.
 *
 * This file is part of the gdeck Library.
 *
 * The Gnome Library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * The Gnome Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with the Gnome Library; see the file COPYING.LIB.  If not,
 * write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef __GDECK_CARD_BASE_H__
#define __GDECK_CARD_BASE_H__


#include <libgnomeui/gnome-canvas-rect-ellipse.h>

#include <libart_lgpl/art_svp.h>

#define GDECK_TYPE_CARD_BASE            (gdeck_card_base_get_type ())
#define GDECK_CARD_BASE(obj)            (GTK_CHECK_CAST ((obj), GDECK_TYPE_CARD_BASE, GDeckCardBase))
#define GDECK_CARD_BASE_CLASS(klass)    (GTK_CHECK_CLASS_CAST ((klass), GDECK_TYPE_CARD_BASE, GDeckCardBaseClass))
#define GDECK_IS_CARD_BASE(obj)         (GTK_CHECK_TYPE ((obj), GDECK_TYPE_CARD_BASE))
#define GDECK_IS_CARD_BASE_CLASS(klass) (GTK_CHECK_CLASS_TYPE ((klass), GDECK_TYPE_CARD_BASE))


typedef struct _GDeckCardBase GDeckCardBase;
typedef struct _GDeckCardBaseClass GDeckCardBaseClass;

struct _GDeckCardBase {
	GnomeCanvasGroup parent;

	/* Antialiased specific stuff */
	ArtSVP *fill_svp;		/* The SVP for the filled shape */
	ArtSVP *outline_svp;		/* The SVP for the outline shape */

	/* Geometry Management */
	gdouble x, y;
	gdouble radius;
	gdouble width;
	gdouble height;
	guint fill_color;		/* Fill color, RGBA */
	guint outline_color;		/* Outline color, RGBA */

	/* Configuration flags */
	unsigned int fill_set : 1;	/* Is fill color set? */
	unsigned int outline_set : 1;	/* Is outline color set? */
};

struct _GDeckCardBaseClass {
	GnomeCanvasGroupClass parent_class;
};

/* Standard Gtk function */
GtkType gdeck_card_base_get_type (void);

#endif
