#include "gdeck.h"
#include "gdeck-slot.h"
#include "gdeck-table.h"

static void gdeck_table_init       (GDeckTable      *re);
static void gdeck_table_class_init (GDeckTableClass *class);
static void gdeck_table_destroy    (GtkObject       *object);


static GnomeCanvasClass *parent_class;

GtkType
gdeck_table_get_type (void)
{
	static GtkType table_type = 0;

	if (!table_type) {
		GtkTypeInfo table_info = {
			"GDeckTable",
			sizeof (GDeckTable),
			sizeof (GDeckTableClass),
			(GtkClassInitFunc) gdeck_table_class_init,
			(GtkObjectInitFunc) gdeck_table_init,
			NULL, /* reserved_1 */
			NULL, /* reserved_2 */
			(GtkClassInitFunc) NULL
		};

		table_type = gtk_type_unique (GNOME_TYPE_CANVAS, &table_info);
	}

	return table_type;
}

static void
gdeck_table_init (GDeckTable *table)
{
	GNOME_CANVAS (table)->aa = 1;
	table->x_offset = 0;
	table->y_offset = 0;
	table->event = GDECK_EVENT_NONE;
}

static void
gdeck_table_class_init (GDeckTableClass *class)
{
	GtkObjectClass *object_class;

	object_class = (GtkObjectClass *) class;

	parent_class = gtk_type_class (gnome_canvas_get_type ());

	object_class->destroy = gdeck_table_destroy;
}

static void
gdeck_table_destroy (GtkObject *object)
{

}

GtkWidget *
gdeck_table_new (void)
{
	GDeckTable *retval;

	retval = (GDeckTable *) gtk_type_new (gdeck_table_get_type ());
	return GTK_WIDGET (retval);
}

void
gdeck_table_set_background (GDeckTable *table,
			    GdkPixbuf  *bg)
{

}

void
gdeck_table_add_slot (GDeckTable *table,
		      GDeckSlot  *slot,
		      gdouble     x,
		      gdouble     y)
{
	g_return_if_fail (table != NULL);
	g_return_if_fail (GDECK_IS_TABLE (table));
	g_return_if_fail (slot != NULL);

	gnome_canvas_item_set (GNOME_CANVAS_ITEM (slot->group),
			       "x", (gdouble) x,
			       "y", (gdouble) y,
			       NULL);
	table->slots = g_slist_append (table->slots, slot);
}

GDeckSlot *
gdeck_table_find_slot (GDeckTable *table,
		       gdouble     x,
		       gdouble     y)
{
	GSList *tmp;

	g_return_val_if_fail (table != NULL, NULL);
	g_return_val_if_fail (GDECK_IS_TABLE (table), NULL);

	for (tmp = table->slots; tmp; tmp = tmp->next) {
		if (gdeck_slot_contains_point ((GDeckSlot *)tmp->data, x, y))
			return (GDeckSlot *) tmp->data;
	}
	return NULL;
}

