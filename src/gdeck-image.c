#include "gdeck.h"
#include "gdeck-image.h"
#include <gdk-pixbuf/gdk-pixbuf.h>
#include <libgnome/gnome-util.h>

typedef struct _GDeckDeck GDeckDeck;
struct _GDeckDeck {
	/* the current slot filenames */
	gchar *current_suit;
	gchar *current_numbers;
	gchar *current_slot;
	gchar *current_back;
	gchar *current_royalty;

	/* the actual pixbufs */
	GdkPixbuf *suits[GDECK_NUM_SUITS][NUM_SIZES];
	GdkPixbuf *numbers[10];
	GdkPixbuf *royal[GDECK_NUM_SUITS][3];
	GdkPixbuf *card_back;
	GdkPixbuf *blank_card;
	GdkPixbuf *slot;
};

static GDeckDeck deck;
static gboolean has_initted = FALSE;

void
gdeck_image_init (void)
{
	if (has_initted)
		return;

	deck.current_suit = gnome_pixmap_file ("gdeck/suit/default.png");
	deck.current_numbers = gnome_pixmap_file ("gdeck/numbers/default.png");
	deck.current_slot = gnome_pixmap_file ("gdeck/slot/default.png");
	deck.current_back = gnome_pixmap_file ("gdeck/back/default.png");
	deck.current_royalty = gnome_pixmap_file ("gdeck/royalty/default.png");

	deck.card_back = gdk_pixbuf_new_from_file (deck.current_back);
	has_initted = TRUE;
}

GdkPixbuf *
gdeck_image_get_suit (GDeckSuit suit, GDeckSizes size)
{
	g_return_val_if_fail (suit < GDECK_NUM_SUITS && size >= 0, NULL);
	g_return_val_if_fail (size < NUM_SIZES && size >= 0, NULL);

	if (deck.suits[suit][size] == NULL) {
		
	}
	return NULL;
}

GdkPixbuf *
gdeck_image_get_back (void)
{
	return deck.card_back;
}

GdkPixbuf *
gdeck_image_get_value (GDeckValue value)
{
	return NULL;
}

GdkPixbuf *
gdeck_image_get_slot (GDeckSlotType type)
{
	return NULL;
}

