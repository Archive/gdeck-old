/*
 * Copyright (C) 2000 Jonathan Blandford  <jrb@redhat.com>
 * All rights reserved.
 *
 * This file is part of the gdeck Library.
 *
 * The Gnome Library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * The Gnome Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with the Gnome Library; see the file COPYING.LIB.  If not,
 * write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef __GDECK_CARD_H__
#define __GDECK_CARD_H__

#ifdef __cplusplus
extern "C" {
#pragma }
#endif

#include "gdeck.h"
#include "gdeck-card-item.h"

struct _GDeckCard {
	GDeckSlot *slot;
	GDeckSuit suit;
	GDeckValue value;
	GnomeCanvasItem *item;
	guint face_up : 1;
	gpointer data;
};

GDeckCard       *gdeck_card_new          (GDeckSuit         suit,
					  GDeckValue        value,
					  gboolean          face_up);
GList           *gdeck_deck_new          (gboolean          face_up);
GDeckSuit        gdeck_card_get_suit     (GDeckCard        *card);
GDeckValue       gdeck_card_get_value    (GDeckCard        *card);
gboolean         gdeck_card_get_face_up  (GDeckCard        *card);
GDeckSlot       *gdeck_card_get_slot     (GDeckCard        *card);
void             gdeck_card_set_face_up  (GDeckCard        *card,
					  gboolean          face_up);
void             gdeck_card_flip         (GDeckCard        *card);
GnomeCanvasItem *gdeck_card_create_item  (GDeckCard        *card,
					  GnomeCanvasGroup *group);
GnomeCanvasItem *gdeck_card_ensure_item  (GDeckCard        *card,
					  GnomeCanvasGroup *group);
GnomeCanvasItem *gdeck_card_get_item     (GDeckCard        *card);
void             gdeck_card_destroy_item (GDeckCard        *card);

#ifdef __cplusplus
}
#endif

#endif

