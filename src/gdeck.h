#ifndef __GDECK_H__
#define __GDECK_H__

#include <libgnomeui/gnome-canvas.h>
#include <gdk-pixbuf/gdk-pixbuf.h>


/* ENUMS */
typedef enum {
	SMALL_IMAGE,
	MEDIUM_IMAGE,
	LARGE_IMAGE,
	NUM_SIZES
} GDeckSizes;

typedef enum
{
	GDECK_CLUB,
	GDECK_DIAMOND,
	GDECK_HEART,
	GDECK_SPADE,
	GDECK_NUM_SUITS
} GDeckSuit;

typedef enum
{
         GDECK_ACE = 1,
	 GDECK_TWO,
	 GDECK_THREE,
	 GDECK_FOUR,
	 GDECK_FIVE,
	 GDECK_SIX,
	 GDECK_SEVEN,
	 GDECK_EIGHT,
	 GDECK_NINE,
	 GDECK_TEN,
	 GDECK_JACK,
	 GDECK_QUEEN,
	 GDECK_KING,
	 GDECK_ACE_HIGH,
	 GDECK_JOKER_1,
	 GDECK_JOKER_2,
	 GDECK_NUM_DECKS
} GDeckValue;

typedef enum
{
        GDECK_SLOT_NORMAL,
	GDECK_SLOT_RIGHT,
	GDECK_SLOT_PARTIAL_RIGHT,
	GDECK_SLOT_DOWN,
	GDECK_SLOT_PARTIAL_DOWN,
	GDECK_SLOT_LEFT,
	GDECK_SLOT_PARTIAL_LEFT,
	GDECK_SLOT_UP,
	GDECK_SLOT_PARTIAL_UP
} GDeckSlotType;

typedef struct _GDeckSlot GDeckSlot;
typedef struct _GDeckCard GDeckCard;


void    gdeck_init        (void);
gdouble gdeck_card_width  (void);
gdouble gdeck_card_height (void);

#endif
