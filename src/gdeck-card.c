/*
 * Copyright (C) 2000 Jonathan Blandford  <jrb@redhat.com>
 * All rights reserved.
 *
 * This file is part of the gdeck Library.
 *
 * The Gnome Library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * The Gnome Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with the Gnome Library; see the file COPYING.LIB.  If not,
 * write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include <gtk/gtksignal.h>
#include "gdeck-table.h"
#include "gdeck-card.h"
#include "gdeck-card-item.h"

static gint card_event (GnomeCanvasItem *item, GdkEvent *event, gpointer data);
static gint card_group_event (GnomeCanvasItem *item, GdkEvent *event, gpointer data);


GDeckCard *
gdeck_card_new (GDeckSuit  suit,
		GDeckValue value,
		gboolean   face_up)
{
	GDeckCard *retval;
	retval = g_new (GDeckCard, 1);

	retval->suit = suit;
	retval->value = value;
	retval->item = NULL;
	retval->face_up = face_up;

	return retval;
}

GList *
gdeck_deck_new (gboolean face_up)
{
	GList *retval = NULL;
	GDeckSuit suit = GDECK_CLUB;
	GDeckValue value = GDECK_TWO;

	while (1) {
		retval = g_list_prepend (retval, gdeck_card_new (suit, value, face_up));
		if (suit == GDECK_SPADE && value == GDECK_ACE_HIGH)
			break;
		if (value == GDECK_ACE_HIGH) {
			value = GDECK_TWO;
			suit++;
		} else {
			value++;
		}
	}

	return retval;			
}

GDeckSuit
gdeck_card_get_suit (GDeckCard *card)
{
	return card->suit;
}

GDeckValue
gdeck_card_get_value (GDeckCard *card)
{
	return card->value;
}

gboolean
gdeck_card_get_face_up (GDeckCard *card)
{
	return card->face_up;
}

GDeckSlot *
gdeck_card_get_slot (GDeckCard *card)
{
	g_return_val_if_fail (card != NULL, NULL);

	return card->slot;
}

void
gdeck_card_set_face_up (GDeckCard *card,
			gboolean   face_up)
{
	if (card->face_up == face_up)
		return;

	card->face_up = face_up;
	if (card->item != NULL)
		;
}

void
gdeck_card_flip (GDeckCard *card)
{
	gdeck_card_set_face_up (card, !card->face_up);
}

GnomeCanvasItem *
gdeck_card_create_item (GDeckCard  	 *card,
			GnomeCanvasGroup *group)
{
	if (card->item)
		gtk_object_destroy (GTK_OBJECT (card->item));

	card->item = gnome_canvas_item_new (group,
					    gdeck_card_item_get_type (),
					    NULL);
	GDECK_CARD_ITEM (card->item)->card = card;
	gtk_signal_connect (GTK_OBJECT (card->item), "event", GTK_SIGNAL_FUNC (card_event), card);
	return card->item;
}

GnomeCanvasItem *
gdeck_card_ensure_item (GDeckCard  	 *card,
			GnomeCanvasGroup *group)
{
	if (card->item) {
		gnome_canvas_item_reparent (card->item, group);
		return card->item;
	}

	gdeck_card_create_item (card, group);
	return card->item;
}

GnomeCanvasItem *
gdeck_card_get_item (GDeckCard *card)
{
	return card->item;
}

void
gdeck_card_destroy_item (GDeckCard *card)
{
	if (card->item) {
		gtk_object_destroy (GTK_OBJECT (card->item));
		card->item = NULL;
	}
}

static void
set_up_card_group (GnomeCanvasItem *item, GdkEvent *event, gpointer data)
{
	GDeckTable *table;
	GList *card_stack, *tmp;
	GnomeCanvasGroup *group;
	GDeckCard *card, *tmp_card;
	GdkCursor *fleur;
	GDeckSlot *slot;

	table = GDECK_TABLE (item->canvas);
	card = (GDeckCard *) data;
	slot = gdeck_card_get_slot (card);

	group = GNOME_CANVAS_GROUP (gnome_canvas_item_new (gnome_canvas_root (item->canvas),
							   GNOME_TYPE_CANVAS_GROUP,
							   NULL));

	gtk_signal_connect (GTK_OBJECT (group), "event", GTK_SIGNAL_FUNC (card_group_event), slot);
	/* we want to move the card */
	fleur = gdk_cursor_new (GDK_FLEUR);
	gnome_canvas_item_grab (GNOME_CANVAS_ITEM (group),
				GDK_POINTER_MOTION_MASK | GDK_BUTTON_RELEASE_MASK,
				fleur,
				event->button.time);
	gdk_cursor_destroy (fleur);
	table->event = GDECK_EVENT_MOVING;
	card_stack = gdeck_slot_remove_card_stack (slot, card);
	for (tmp = card_stack; tmp; tmp = tmp->next) {
		tmp_card = (GDeckCard *) tmp->data;
		tmp_card->slot = NULL;
		gnome_canvas_item_reparent (gdeck_card_get_item (tmp_card),
					    group);
	}
	gnome_canvas_item_show (GNOME_CANVAS_ITEM (group));
}

static gint
card_group_event (GnomeCanvasItem *item, GdkEvent *event, gpointer data)
{
	GDeckTable *table;
	gdouble item_x, item_y;
	gdouble x_offset = 0.0, y_offset = 0.0;
	GDeckSlot *drop_slot = NULL;
	GList *list, *tmp;

	table = GDECK_TABLE (item->canvas);

	item_x = event->button.x;
	item_y = event->button.y;
	gnome_canvas_item_w2i (item->parent, &item_x, &item_y);

	switch (event->type) {
	case GDK_MOTION_NOTIFY:
		if ((event->motion.state & GDK_BUTTON1_MASK) &&
		    table->event == GDECK_EVENT_MOVING) {
			gnome_canvas_item_move (item,
 						item_x - table->x_offset,
						item_y - table->y_offset);
			table->x_offset = item_x;
			table->y_offset = item_y;
		}
		break;

	case GDK_BUTTON_RELEASE:
		/* Where do we drop? */
		x_offset = item_x - GNOME_CANVAS_ITEM (item)->x1;
		y_offset = item_y - GNOME_CANVAS_ITEM (item)->y1;

		drop_slot = gdeck_table_find_slot (table, event->button.x, event->button.y);
		if (drop_slot == NULL)
			drop_slot = (GDeckSlot *) data;
		list = NULL;
		for (tmp = GNOME_CANVAS_GROUP (item)->item_list_end; tmp; tmp = tmp->prev)
			list = g_list_prepend (list, GDECK_CARD_ITEM (tmp->data)->card);
		gdeck_slot_add_card_stack (drop_slot, list);

		/* Clean up the moving stack */
		if (table->event == GDECK_EVENT_MOVING)
			gnome_canvas_item_ungrab (item, event->button.time);
		table->event = GDECK_EVENT_NONE;
		gtk_object_destroy (GTK_OBJECT (item));
		break;

	default:
		break;
	}

	return FALSE;
}

static gint
card_event (GnomeCanvasItem *item, GdkEvent *event, gpointer data)
{
	GDeckTable *table;
	double item_x, item_y;

	table = GDECK_TABLE (item->canvas);

	/* item_[xy] are relative to the slot group */
	item_x = event->button.x;
	item_y = event->button.y;
	gnome_canvas_item_w2i (item->parent, &item_x, &item_y);

	switch (event->type) {
	case GDK_BUTTON_PRESS:
		if (event->button.button == 1) {
			if (table->event == GDECK_EVENT_NONE) {
				table->x_offset = item_x;
				table->y_offset = item_y;
				table->event = GDECK_EVENT_BUTTON_PRESS;
			};
		} else if (event->button.button == 3) {
			
		}
		break;
	case GDK_MOTION_NOTIFY:
		if ((event->motion.state & GDK_BUTTON1_MASK) &&
		    table->event == GDECK_EVENT_BUTTON_PRESS) {
			if ((item_x + 3 < table->x_offset) ||
			    (item_x - 3 > table->x_offset) ||
			    (item_y + 3 < table->y_offset) ||
			    (item_y - 3 > table->y_offset)) {
				set_up_card_group (item, event, data);
			}
		}
		break;
	default:
		break;
	}

	return FALSE;
}
