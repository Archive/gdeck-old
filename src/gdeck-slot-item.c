#include "gdeck-slot-item.h"
#include "gdeck-card-item.h"

enum {
	ARG_0,
	ARG_VISIBLE,
};

#define GDECK_GET_SLOT_CLASS(gdeck) (GDECK_SLOT_ITEM_CLASS(GTK_OBJECT(gdeck)->klass))
static void gdeck_slot_item_init       (GDeckSlotItem       *slot);
static void gdeck_slot_item_class_init (GDeckSlotItemClass  *class);
static void gdeck_slot_item_destroy    (GtkObject       *object);
static void gdeck_slot_item_set_arg    (GtkObject       *object,
				   GtkArg          *arg,
				   guint            arg_id);
static void gdeck_slot_item_get_arg    (GtkObject       *object,
				   GtkArg          *arg,
				   guint            arg_id);
static void gdeck_slot_item_render     (GnomeCanvasItem *item,
				   GnomeCanvasBuf  *buf);

static GDeckCardBaseClass *parent_class;

GtkType
gdeck_slot_item_get_type (void)
{
	static GtkType slot_type = 0;

	if (!slot_type) {
		GtkTypeInfo slot_info = {
			"GDeckSlotItem",
			sizeof (GDeckSlotItem),
			sizeof (GDeckSlotItemClass),
			(GtkClassInitFunc) gdeck_slot_item_class_init,
			(GtkObjectInitFunc) gdeck_slot_item_init,
			NULL, /* reserved_1 */
			NULL, /* reserved_2 */
			(GtkClassInitFunc) NULL
		};

		slot_type = gtk_type_unique (GDECK_TYPE_CARD_BASE, &slot_info);
	}

	return slot_type;
}

static void
gdeck_slot_item_init (GDeckSlotItem *slot)
{
	slot->visible = TRUE;
}

static void
gdeck_slot_item_class_init (GDeckSlotItemClass *class)
{
	GtkObjectClass *object_class;
	GnomeCanvasItemClass *item_class;

	object_class = (GtkObjectClass *) class;
	item_class = (GnomeCanvasItemClass *) class;

	parent_class = gtk_type_class (GDECK_TYPE_CARD_BASE);

	gtk_object_add_arg_type ("GDeckSlotItem::visible", GTK_TYPE_BOOL, GTK_ARG_READWRITE, ARG_VISIBLE);

	object_class->destroy = gdeck_slot_item_destroy;
	object_class->set_arg = gdeck_slot_item_set_arg;
	object_class->get_arg = gdeck_slot_item_get_arg;

	item_class->render = gdeck_slot_item_render;
	class->offset_val = 15;
}

static void
gdeck_slot_item_destroy (GtkObject *object)
{
	GDeckSlotItem *slot;

	slot = GDECK_SLOT_ITEM (object);
	
	if (GTK_OBJECT_CLASS (parent_class)->destroy)
		(* GTK_OBJECT_CLASS (parent_class)->destroy) (object);
}

static void
gdeck_slot_item_set_arg (GtkObject *object,
			 GtkArg    *arg,
			 guint      arg_id)
{
	GnomeCanvasItem *item;
	GDeckSlotItem *slot;

	item = GNOME_CANVAS_ITEM (object);
	slot= GDECK_SLOT_ITEM (object);

	switch (arg_id) {
	case ARG_VISIBLE:
		slot->visible = GTK_VALUE_BOOL (*arg);
		gnome_canvas_item_request_update (item);
		break;
	default:
		break;
	}
}

static void
gdeck_slot_item_get_arg (GtkObject *object,
			 GtkArg    *arg,
			 guint      arg_id)
{
	GDeckSlotItem *card;

	card = GDECK_SLOT_ITEM (object);

	switch (arg_id) {
	case ARG_VISIBLE:
		GTK_VALUE_BOOL (*arg) = card->visible;
		break;
	default:
		arg->type = GTK_TYPE_INVALID;
		break;
	}
}


static void
gdeck_slot_item_render (GnomeCanvasItem *item,
		   GnomeCanvasBuf  *buf)
{
	GDeckSlotItem *slot;

	slot = GDECK_SLOT_ITEM (item);

	if (slot->visible && GNOME_CANVAS_ITEM_CLASS (parent_class)->render)
		(* GNOME_CANVAS_ITEM_CLASS (parent_class)->render) (item, buf);
}
